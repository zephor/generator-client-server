# generator-client-server [![Build Status](https://secure.travis-ci.org/zephor/generator-client-server.png?branch=master)](https://travis-ci.org/zephor/generator-client-server)

> [Yeoman](http://yeoman.io) generator

A simple yeoman generator for a client-server project.

## License

MIT
