'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');


var ClientServerGenerator = yeoman.generators.Base.extend({
    init: function () {
        this.pkg = require('../package.json');

        this.on('end', function () {
            if (!this.options['skip-install']) {
            this.installDependencies();
            }
        });
    },

    askFor: function () {
        var done = this.async();
        var prompts = [{
            name: 'appName',
            message: 'What would you like to name you app?'
        }];

        this.prompt(prompts, function (props) {
            this.appName = props.appName;
            this.fileFriendlyName = this.appName.toLowerCase().replace(/\s/g, '-');
            done();
        }.bind(this));
    },

    makeClient: function () {
        var clientFileName = this.fileFriendlyName + '-client.js';
        this.mkdir('client');
        this.mkdir('client/libs');
        this.template('_client.js', 'client/' + clientFileName);
    },
    makeServer: function() {
        var serverFileName = this.fileFriendlyName + '-server.js';
        this.mkdir('server');
        this.mkdir('server/templates');
        this.mkdir('server/models');
        this.mkdir('server/routes');
        this.template('_server.js', 'server/' + serverFileName);
        this.template('_router.js', 'server/router.js');
        this.template('_authentication.js', 'server/authentication.js');
        this.template('_database.js', 'server/database.js');
    },

    projectfiles: function () {
        this.copy('_package.json', 'package.json');
        this.copy('_bower.json', 'bower.json');
        this.copy('_bowerrc', '.bowerrc');
    }
});

module.exports = ClientServerGenerator;